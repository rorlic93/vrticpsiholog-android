package com.example.rastimirorlic.vrticpsiholog.LoginScreen;

/**
 * Created by rastimir.orlic on 3.5.2017..
 */

public interface ILoginView {

    void showProgress();
    void hideProgress();
    void setUsernameError();
    void setPasswordError();
    void startDownload();
    void showAlert(String message);

}
