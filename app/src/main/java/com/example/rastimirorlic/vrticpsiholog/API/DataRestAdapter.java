package com.example.rastimirorlic.vrticpsiholog.API;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rastimir on 07.05.2017..
 */

public class DataRestAdapter {

    protected Context mContext;
    protected Retrofit mRestAdapter;
    protected DataApi dataApi;

    final String BASE_URL = "http://127.0.0.1/";

    public DataRestAdapter(Context context){
        mContext = context;

        mRestAdapter = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        dataApi = mRestAdapter.create(DataApi.class);
    }

    public void testDataApi(String username, String password){
        String credentials = username + ":" + password;
        String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        String app_json = "application/json";

        Call<ResponseBody> myCall = dataApi.getAllDataFromApi(basic);
        myCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if(response.isSuccessful()){
                        String responseStr = response.body().string();
                        getDataFromJson(responseStr);
                        Log.i("RESPONSE_STR", responseStr);
                    }else{
                        Log.i("RESPONSE_STR", "not sucessful");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getDataFromJson(String movieJsonResponse) throws JSONException{
        JSONObject dataJSON = new JSONObject(movieJsonResponse);
        JSONArray jsonArray = dataJSON.getJSONArray("djeca");

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject dijete = jsonArray.getJSONObject(i);
            Log.i("JSON_OBJECT", dijete.getString("ime"));
        }

//        long movieID;
//
//        for(int i = 0; i < movieArray.length(); i++){
//            JSONObject movie = movieArray.getJSONObject(i);
//
//            movieID = addMovie(movie.getString("id"), movie.getString("original_title"),
//                    movie.getString("overview"), movie.getString("poster_path"),
//                    movie.getString("backdrop_path"), movie.getDouble("vote_average"),
//                    movie.getString("release_date"), movie.getDouble("popularity"), 0);
//            Log.i("INSERTED MOVIE ID" , String.valueOf(movieID));
//
//        }
    }

}
