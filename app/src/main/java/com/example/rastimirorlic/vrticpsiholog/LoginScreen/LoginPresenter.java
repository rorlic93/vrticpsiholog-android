package com.example.rastimirorlic.vrticpsiholog.LoginScreen;

/**
 * Created by rastimir.orlic on 3.5.2017..
 */

public class LoginPresenter implements ILoginPresenter, LoginModel.onLoginFinishedListener{

    ILoginView mLoginView;
    ILoginModel mLoginModel;


    public LoginPresenter(ILoginView mLoginView) {
        this.mLoginView = mLoginView;
        mLoginModel = new LoginModel();
    }

    @Override
    public void validateCredentials(String username, String password) {
        if(mLoginView != null){
            mLoginView.showProgress();
            mLoginModel.login(username, password, this);
        }
    }

    @Override
    public void onDestroy() {
        if(mLoginView != null){
            mLoginView = null;
        }
    }

    @Override
    public void onUsernameError() {
        if(mLoginView != null){
            mLoginView.hideProgress();
            mLoginView.setUsernameError();
        }
    }

    @Override
    public void onPasswordError() {
        if(mLoginView != null){
            mLoginView.hideProgress();
            mLoginView.setPasswordError();
        }
    }

    @Override
    public void onSuccess() {
        if(mLoginView != null){
            mLoginView.hideProgress();
            mLoginView.startDownload();
        }
    }

    @Override
    public void onFailure(String message) {
        if(mLoginView != null){
            mLoginView.hideProgress();
            mLoginView.showAlert(message);
        }
    }
}
