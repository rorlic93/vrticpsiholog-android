package com.example.rastimirorlic.vrticpsiholog.LoginScreen;

/**
 * Created by rastimir.orlic on 3.5.2017..
 */

public interface ILoginModel {

    interface onLoginFinishedListener{
        void onUsernameError();
        void onPasswordError();
        void onSuccess();
        void onFailure(String message);
    }

    void login(String username, String password, onLoginFinishedListener listener);
    void saveUser(String username, String password);

}
