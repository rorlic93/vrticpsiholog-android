package com.example.rastimirorlic.vrticpsiholog.LoginScreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rastimirorlic.vrticpsiholog.MainScreen.MainActivity;
import com.example.rastimirorlic.vrticpsiholog.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements ILoginView{

    @BindView(R.id.input_email)
    EditText input_email;
    @BindView(R.id.input_password)
    EditText input_password;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    private ILoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        mLoginPresenter = new LoginPresenter(this);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginPresenter.validateCredentials(input_email.getText().toString().trim(), input_password.getText().toString().trim());
            }
        });

    }

    @Override
    public void showProgress() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress_bar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setUsernameError() {
        input_email.setError("Username blank");
    }

    @Override
    public void setPasswordError() {
        input_password.setError("Password blank");
    }

    @Override
    public void startDownload() {
        Toast.makeText(this, "STARTING DOWNLOAD...", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.onDestroy();
    }
}
