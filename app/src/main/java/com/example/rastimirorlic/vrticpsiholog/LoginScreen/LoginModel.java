package com.example.rastimirorlic.vrticpsiholog.LoginScreen;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.example.rastimirorlic.vrticpsiholog.model.User;

/**
 * Created by rastimir.orlic on 3.5.2017..
 */

public class LoginModel implements ILoginModel{
    @Override
    public void login(String username, String password, onLoginFinishedListener listener) {
        if (TextUtils.isEmpty(username)) {
            listener.onUsernameError();
        } else if (TextUtils.isEmpty(password)) {
            listener.onPasswordError();
        }else{
            postToServer(username, password, listener);
        }
    }

    @Override
    public void saveUser(String username, String password) {
        User user = new User(username, password);
        Log.i("USER", user.getUsername() + ", " + user.getPassword());
    }

    private void postToServer(final String username, final String password, final onLoginFinishedListener listener){
        //Todo: ZAHTJEV NA SERVER
        boolean success = false;

        if(username.equals("orlicrastimir@gmail.com") && password.equals("12345")){
            success = true;
        }

        if(success){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    saveUser(username, password);
                    listener.onSuccess();
                }
            }, 3000);
        }else{
            listener.onFailure("These credentials are not valid");
        }
    }

}
