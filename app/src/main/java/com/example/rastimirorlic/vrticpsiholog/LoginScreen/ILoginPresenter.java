package com.example.rastimirorlic.vrticpsiholog.LoginScreen;

/**
 * Created by rastimir.orlic on 3.5.2017..
 */

public interface ILoginPresenter {

    void validateCredentials(String username, String password);
    void onDestroy();

}
