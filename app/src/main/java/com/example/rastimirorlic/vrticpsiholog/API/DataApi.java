package com.example.rastimirorlic.vrticpsiholog.API;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Rastimir on 07.05.2017..
 */

public interface DataApi {
    @GET("/api/public/info")
    Call<ResponseBody> getAllDataFromApi(
            @Header("Authorization") String basic
    );
}
